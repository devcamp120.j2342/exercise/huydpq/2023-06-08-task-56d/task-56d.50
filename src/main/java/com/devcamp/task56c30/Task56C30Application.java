package com.devcamp.task56c30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56C30Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56C30Application.class, args);
	}

}
