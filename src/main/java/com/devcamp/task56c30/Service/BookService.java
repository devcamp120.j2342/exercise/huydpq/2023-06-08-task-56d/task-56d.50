package com.devcamp.task56c30.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task56c30.models.Book;

@Service
public class BookService {
    @Autowired
    private AuthorService authorService;

    public ArrayList<Book> getAllBook() {
        ArrayList<Book> arrayListBook = new ArrayList<>();
        Book book1 = new Book("book1", authorService.getAuthor1(), 10.000, 1);
        Book book2 = new Book("book2", authorService.getAuthor2(), 12.000, 2);
        Book book3 = new Book("book3", authorService.getAuthor3(), 14.000, 3);
        arrayListBook.add(book1);
        arrayListBook.add(book2);
        arrayListBook.add(book3);

        return arrayListBook;
    }

    public Book fillterBook(int index) {
        ArrayList<Book> listBook = getAllBook();

        if (index < 0 || index >= listBook.size()) {
            return null;
        }
        return listBook.get(index);

    }
}
