package com.devcamp.task56c30.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56c30.models.Author;

@Service
public class AuthorService {
    Author author1 = new Author("huy", "huy@gmail.com", 'a');
    Author author2 = new Author("tu", "tu@gmail.com", 'b');

    Author author3 = new Author("hùng", "hung@gmail.com", 'c');
    Author author4 = new Author("linh", "linh@gmail.com", 'd');

    Author author5 = new Author("nga", "nga@gmail.com", 'e');
    Author author6 = new Author("tiên", "tien@gmail.com", 'f');

    public ArrayList<Author> getAuthor1(){
       ArrayList<Author> authors1 = new ArrayList<>();
       authors1.add(author1);
        authors1.add(author2);
        
        return authors1;
    }

    public ArrayList<Author> getAuthor2(){
       ArrayList<Author> authors2 = new ArrayList<>();
       authors2.add(author3);
        authors2.add(author4);
        
        return authors2;
    }
    public ArrayList<Author> getAuthor3(){
       ArrayList<Author> authors3 = new ArrayList<>();
       authors3.add(author5);
        authors3.add(author6);
        
        return authors3;
    }

    public Author fillterAuthorEmail(String email){
         ArrayList<Author> allAuthors = new ArrayList<>();
         allAuthors.add(author1);
        allAuthors.add(author2);
        allAuthors.add(author3);
        allAuthors.add(author4);
        allAuthors.add(author5);
        allAuthors.add(author6);

        Author author = new Author();
        for ( Author AuthorElement : allAuthors) {
            if(AuthorElement.getEmail().equals(email)){
                author = AuthorElement;
            }
        }
        return author;

    }
    public Author fillterAuthorGender(char gender){
         ArrayList<Author> allAuthors = new ArrayList<>();
         allAuthors.add(author1);
        allAuthors.add(author2);
        allAuthors.add(author3);
        allAuthors.add(author4);
        allAuthors.add(author5);
        allAuthors.add(author6);

        Author author = new Author();
        for ( Author AuthorElement : allAuthors) {
            if(AuthorElement.getGender() == gender){
                author = AuthorElement;
            }
        }
        return author;

    }

}
