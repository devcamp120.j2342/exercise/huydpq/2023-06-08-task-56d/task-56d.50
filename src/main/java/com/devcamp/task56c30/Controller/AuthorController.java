package com.devcamp.task56c30.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c30.Service.AuthorService;
import com.devcamp.task56c30.models.Author;

@RestController

public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorEmail(@RequestParam(required = true, name = "code")String email ){
        Author author = authorService.fillterAuthorEmail(email);

        return author;
    }

    @GetMapping("/author-gender")
    public Author getAuthorEmail(@RequestParam(required = true, name = "code")char gender ){
        Author author = authorService.fillterAuthorGender(gender);

        return author;
    }
}
