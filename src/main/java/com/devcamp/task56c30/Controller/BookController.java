package com.devcamp.task56c30.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c30.Service.BookService;
import com.devcamp.task56c30.models.Book;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping ("/book")
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBooks = bookService.getAllBook();

        return allBooks;
    }

    @GetMapping ("/book-quantity")
    public ArrayList<Book> geBooksQty(@RequestParam(required = true, name = "code") int quantityNumber ){
        ArrayList<Book> allBooks = bookService.getAllBook();

        ArrayList <Book> books = new ArrayList<>(1);
        for (Book bookElement : allBooks) {
            if(bookElement.getQty() >= quantityNumber)
            books.add(bookElement);
           
        }
        return books;
    }

    @GetMapping ("/books/{bookId}")
    public Book bookFiller(@PathVariable int bookId ){
        
        return bookService.fillterBook(bookId);
    }
}
